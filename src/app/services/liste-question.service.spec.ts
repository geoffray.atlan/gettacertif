import { TestBed, inject } from '@angular/core/testing';

import { ListeQuestionService } from './liste-question.service';

describe('ListeQuestionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListeQuestionService]
    });
  });

  it('should be created', inject([ListeQuestionService], (service: ListeQuestionService) => {
    expect(service).toBeTruthy();
  }));
});
