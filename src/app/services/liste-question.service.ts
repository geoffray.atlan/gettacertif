import { Injectable, Input } from '@angular/core';

@Injectable()
export class ListeQuestionService {

  listeQuestion = [
    {
        "question":"A group of users must be able to associate a time zone with a record. You need to implement the required functionality by using native Microsoft Dynamics 365 features. Which field type should you use?",
        "choix":[
            ["A. option set"],
            ["B. multiple lines of text"],
            ["C. whole number"],
            ["D. lookup"]
        ],
        "solution":"A",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"For which of the following scenarios can you delete an entity?",
        "choix":[
            ["A. The entity is custom and uses a system-defined global option set."],
            ["B. The entity is part of a managed solution and no other items are dependent upon the entity."],
            ["C. The entity is part of an unmanaged solution and another unmanaged entity has a lookup field to the entity on one form."],
            ["D. The entity is system-defined and no other items are dependent upon the entity."]
        ],
        "solution":"A",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You are a subject matter expert for an insurance company. You have a complex business method that is used for settling and releasing claims. You need to create a process in Microsoft Dynamics 365 to assist team members with the claims settlement process. You must limit this process to team members that have a specific security role. What should you implement?",
        "choix":[
            ["A. business process flow (BPF)"],
            ["B. workflows"],
            ["C. custom actions"],
            ["D. dialogs"]
        ],
        "solution":"A",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You are implementing a new business process. The system must send a notification to the manager 24 hours after an opportunity is created based on the date of the next best action. Which type of workflow should you implement?",
        "choix":[
            ["A. synchronous workflow"],
            ["B. asynchronous workflow"],
            ["C. Microsoft Dynamics 365 dialogs"],
            ["D. business process flow (BPF)"]
        ],
        "solution":"A",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You configure a 1:N relationship between two entities and set the cascade rule for deletion to Referential. What effect does deleting the parent record have on any child records?",
        "choix":[
            ["A. The child records remain but the link to the parent record is removed."],
            ["B. The parent record cannot be deleted until all of the child records are first deleted."],
            ["C. The child records are deleted and the link to the parent record shows as deleted."],
            ["D. The child records are deleted along with the parent record."]
        ],
        "solution":"A",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You create a business rule for the Case entity and set the scope to Case. Which action will cause the business rule to run",
        "choix":[
            ["A. The form loads."],
            ["B. An asynchronous workflow updates the record."],
            ["C. An asynchronous workflow creates a record."],
            ["D. The record is saved."]
        ],
        "solution":"A",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You have a Dynamic CRM organization. Currently, users can view 50 records per page. You need to ensure that the users can view 250 records per page. What should you do",
        "choix":[
            ["A. Instruct the users to configure their options."],
            ["B. Instruct the users to create custom views."],
            ["C. Instruct an administrator to configure the view settings, and then to share a view."],
            ["D. Instruct an administrator to configure their options."]
        ],
        "solution":"A",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You implement Microsoft Office 365 groups for a company. You need to ensure that all members of a specific security role can see and use an Office 365 group. Which privilege should you assign to the security role",
        "choix":[
            ["A. ISV Extensions"],
            ["B. Execute Workflow Job"],
            ["C. Act on behalf of another user"],
            ["D. Browse Availability"]
        ],
        "solution":"A",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You need to create a hyperlink field to capture the URL for a contact’s LinkedIn profile. Which data type should you use for the field",
        "choix":[
            ["A. single line of text"],
            ["B. web"],
            ["C. URL"],
            ["D. text area"]
        ],
        "solution":"A",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You need to display data is referenced by a lookup field on a form. Which option should you use",
        "choix":[
            ["A. Quick View"],
            ["B. Associated View"],
            ["C. Public View"],
            ["D. Quick Find View"]
        ],
        "solution":"A",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You need to grant users the ability to read all accounts in Microsoft Dynamics 365. Which access level should you apply to the Read permission for the Account entity",
        "choix":[
            ["A. Organization"],
            ["B. System"],
            ["C. All"],
            ["D. Global"]
        ],
        "solution":"A",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You plan to create a country entity that contains one record for each country in the world. You need to track which countries a contact visited. No additional data will be tracked. Which type of relationship should you create",
        "choix":[
            ["A. a many-to-many (N:N) relationship from the contact to the country"],
            ["B. a one-to-many (1:N) relationship from the country to the contact"],
            ["C. a many-to-many (N:N) relationship from the contact to the country that has a relationship behavior type of Parental"],
            ["D. a one-to-many (1:N) relationship from the contact to the country"]
        ],
        "solution":"A",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You plan to create a field for an entity to capture data in a consistent format. When a user adds a record for the entity, the choices provided to the user for the field often change or become outdated. The choices must not be available for new records. You need to create the field while minimizing the administrative overhead to maintain the field. Which field type should you use",
        "choix":[
            ["A. global option set"],
            ["B. single line of text"],
            ["C. local option set"],
            ["D. lookup"]
        ],
        "solution":"A",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"Your organization has a custom entity that contains 25 records. A specific group of users needs access to the records. You need to ensure that only specified users can access the records and that you can manage access from one place. What should you do",
        "choix":[
            ["A. Add the users to access teams. Grant the teams read and write access for each of the custom entity records."],
            ["B. Ask the owner of the custom entity records to grant specific users read and write access to the records."],
            ["C. Create an owner team that includes the users. Assign the team a new security role which can access only the custom entity."],
            ["D. Modify an existing security role that is common to the users. Grant the role access to the custom entity"]
        ],
        "solution":"A",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"Which two form areas allow you to read from and write to all visible fields? Each answer presents a complete solution.",
        "choix":[
            ["A. Header"],
            ["B. Body"],
            ["C. Navigation"],
            ["D. Footer"]
        ],
        "solution":"AB",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"Which two statements are exclusive to managed solutions and not unmanaged solutions? Each correct answer presents part of the solution.",
        "choix":[
            ["A. You cannot export the solution."],
            ["B. When you remove the solution, all components items included in the solution are removed."],
            ["C. You must define entity assets for every entity that you add to the solution."],
            ["D. You must select a publisher for the solution"]
        ],
        "solution":"AB",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You are a Microsoft Dynamics 365 administrator for a real estate company. The sales team wants to use mobile devices to update opportunities and gain insights about customers while they are out of the office. You need to instruct the sales team on how to install mobile apps for Microsoft Dynamics 365. To which two locations should you direct the sales team members? Each correct answer presents a complete solution",
        "choix":[
            ["A. the Settings (Personal Options) page in Microsoft Dynamics 365"],
            ["B. the Apple iTunes App Store, Google Play App Store, or Windows Store"],
            ["C. a specific Microsoft SharePoint site"],
            ["D. a Microsoft OneDrive for Business folder"]
        ],
        "solution":"AB",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You are configuring auditing for Microsoft Dynamics 365. Which two operations can you audit? Each correct answer presents a complete solution",
        "choix":[
            ["A. Create, update, and delete records."],
            ["B. Create an association between records."],
            ["C. Retrieve records."],
            ["D. Export records."]
        ],
        "solution":"AB",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You have an environment that includes Microsoft Dynamics 365 and Exchange Online. Email messages between Microsoft Dynamics and Exchange are synchronizing correctly. You need to ensure that appointments, contacts, and tasks are synchronized. What are two possible ways to achieve this goal? Each correct answer presents a complete solution",
        "choix":[
            ["A. Configure the default synchronization method for appointments, contacts, and tasks to use server side synchronization."],
            ["B. Configure the default synchronization method for appointments, contacts, and tasks to use Microsoft Dynamics 365 for Outlook."],
            ["C. Connect Microsoft Dynamics 365 to POP3/SMTP servers."],
            ["D. Set up the email router."]
        ],
        "solution":"AB",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You plan to implement a server-based integration between Microsoft Dynamics 365 and SharePoint Online. Which two statements are true? Each correct answer presents a complete solution",
        "choix":[
            ["A. You only need to sign in to Microsoft Dynamics 365 to access documents stored in SharePoint."],
            ["B. SharePoint actions including Alert Me, Download a Copy, and Copy Shortcut are supported."],
            ["C. You do not need to implement a list component."],
            ["D. Users can create folders in Microsoft Dynamics 365"]
        ],
        "solution":"AB",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"For which three scenarios can you enable duplicate detection?Each correct answer presents a complete solution",
        "choix":[
            ["A. when Microsoft Dynamics 365 for Outlook goes from offline to online"],
            ["B. when a record is created or updated"],
            ["C. during data import"],
            ["D. when a record is deleted or deactivated"],
            ["E. when Microsoft Dynamics 365 for Outlook goes from online to offline"]
        ],
        "solution":"ABC",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You have a Microsoft Dynamics 365 tenant. You plan to implement Microsoft Office 365 Groups. You need to ensure that all Office 365 Group features are available. Which three actions should you perform? Each correct answer presents part of the solution",
        "choix":[
            ["A. Configure a Microsoft Exchange Online mailbox for each Microsoft Dynamics 365 user."],
            ["B. Enable the ISV Extensions security privilege."],
            ["C. Configure Yammer to work with Microsoft Dynamics 365."],
            ["D. Enable server-based Microsoft SharePoint in Microsoft Dynamics 365."],
            ["E. Configure the Microsoft SharePoint List component."]
        ],
        "solution":"ABD",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"Which three actions can you perform by using business rules? Each correct answer presents a complete solution.",
        "choix":[
            ["A. Display an error message at the field level."],
            ["B. Set a default field value."],
            ["C. Set the active business process flow (BPF) stage."],
            ["D. Display an error message at the form level."],
            ["E. Clear a field value."]
        ],
        "solution":"ABE",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"Which three statements regarding actions are true? Each correct answer presents a complete solution.",
        "choix":[
            ["A. Actions can be invoked from a custom client application that uses Microsoft Dynamics 365."],
            ["B. Actions can be invoked from JavaScript within the application."],
            ["C. Actions must always be associated with a specified entity."],
            ["D. Actions require some coding to enable invocation from a workflow or dialog."],
            ["E. You can invoke actions from a workflow or dialog without writing code"]
        ],
        "solution":"ABE",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"Which two chart types can you combine with another chart type Each correct answer presents a complete solution",
        "choix":[
            ["A. line"],
            ["B. pie"],
            ["C. column"],
            ["D. funnel"]
        ],
        "solution":"AC",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"Which two entity properties can you disable on a system entity?",
        "choix":[
            ["A. Access Teams"],
            ["B. Notes"],
            ["C. Allow quick create"],
            ["D. Queues"]
        ],
        "solution":"AC",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You create a business rule for the Account entity. The business rule must run when you import account records. Which two conditions must be true? Each correct answer presents a complete solution",
        "choix":[
            ["A. The scope was set to Entity."],
            ["B. The scope was set to Account."],
            ["C. A business rule is active."],
            ["D. A business rule snapshot was successful."]
        ],
        "solution":"AC",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You need to enable auditing for the account entity. Which two actions should you perform? Each correct answer presents a complete solution.",
        "choix":[
            ["A. Start auditing."],
            ["B. Enable change tracking."],
            ["C. Enable auditing for common entities."],
            ["D. Enable auditing for user access."]
        ],
        "solution":"AC",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"To which three data types can you add a custom control?Each correct answer presents a complete solution",
        "choix":[
            ["A. composite "],
            ["B. date and time "],
            ["C. multiple lines of text "],
            ["D. single line text"],
            ["E. lookup"]
        ],
        "solution":"ACD",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"Which three actions can you add to a workflow? Each correct answer presents a complete solution",
        "choix":[
            ["A. Create Record"],
            ["B. Share Record"],
            ["C. Update Record"],
            ["D. Assign Record"],
            ["E. Delete Record"]
        ],
        "solution":"ACD",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You have a Dynamics CRM organization. Currently, when you create entities and fields, they have a prefix of new_. You need to ensure that when you create new entities and fields, they have a prefix of Contoso_. Which two actions should you perform? Each correct answer presents part of the solution",
        "choix":[
            ["A. Change the prefix in the System Settings to Contoso."],
            ["B. Edit the customization prefix field and change the display name to Contoso."],
            ["C. Change the prefix of the publisher associated to the solution to Contoso."],
            ["D. Create a new publisher that has a prefix of Contoso. Associate the new publisher to the exiting solution."]
        ],
        "solution":"AD",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You have a business process named Know Your Customer. Employees use this business process to capture key information about customers. You must implement a business process flow (BPF) in Microsoft Dynamics 365 that represents this business process. You specify the name for the BPF as well as the stage names, entities, and stage category that the BPF will use. You need to complete the BPF implementation. Which three additional components can you configure? Each correct answer presents a complete solution",
        "choix":[
            ["A. Step Required"],
            ["B. Check Conditions"],
            ["C. Wait Conditions"],
            ["D. Step Display Names"],
            ["E. Data Fields"]
        ],
        "solution":"ADE",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"Which form type is used by the Microsoft Dynamics 365 for tablets app?",
        "choix":[
            ["A. Mobile"],
            ["B. Main"],
            ["C. Mobile - Express"],
            ["D. Turbo"]
        ],
        "solution":"B",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"Which of the following chart types can be configured as an organization-owned chart but not as a user-owned chart?",
        "choix":[
            ["A. bar and line "],
            ["B. tag or doughnut "],
            ["C. line and multi-series "],
            ["D. area and funnel"]
        ],
        "solution":"B",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"Which statement regarding alternate keys is true?",
        "choix":[
            ["A. You can define only one alternate key per entity."],
            ["B. Alternate keys are enforced when creating records through the user interface, and during data imports."],
            ["C. You can use date and time fields as part of an alternate key."],
            ["D. Alternate keys provide a warning if there is a matching record, but still allow the record to be saved"]
        ],
        "solution":"B",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"Which statement regarding ordering of items in the mobile navigation menu is ture?",
        "choix":[
            ["A. Menu item placement is governed by mobile offline settings."],
            ["B. Menu item placement is based on the site map."],
            ["C. Most recently used entities appear first."],
            ["D. Menu items are displayed in alphabetical order."]
        ],
        "solution":"B",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You add a lookup for contacts to the case form. You need to display the most recent primary phone number for a contact on the case form while minimizing administrative effort. What should you do",
        "choix":[
            ["A. Use a workflow to copy the phone number to a new field on the case when the contact changes."],
            ["B. Use a quick view form to display the phone number."],
            ["C. Use a web resource to query and display the phone number."],
            ["D. Use field mapping during record creation to copy the phone number to a new field on the case."]
        ],
        "solution":"B",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You administer a Microsoft Dynamics 365 environment for a company. You need to prevent one specific employee from synchronizing data by using Microsoft Dynamics 365 for Outlook without preventing the employee from using Microsoft Dynamics 365 for Outlook. What should you do",
        "choix":[
            ["A. Set the Web Mail Merge settings to None Selected for the individual’s security role."],
            ["B. Set the Sync to Outlook settings to None Selected for the individual’s security role."],
            ["C. Set the Dynamics 365 Address Book settings to None Selected for the individual’s security role."],
            ["D. Set the Use Dynamics 365 App for Outlook to None Selected for the individual’s security role."]
        ],
        "solution":"B",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You are creating a business rule for the account records. The business rule will perform an action if the record has Country set to the United States and City set to either Redmond or San Francisco. You need to identify the minimum number of condition sets required to implement this logic. What should you identify",
        "choix":[
            ["A. 1"],
            ["B. 2"],
            ["C. 3"],
            ["D. 4"]
        ],
        "solution":"B",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You are creating a field for a form. You need to ensure that users can select an Account record or a Contact record. Which data type should you use for the field",
        "choix":[
            ["A. regarding"],
            ["B. customer"],
            ["C. lookup"],
            ["D. option set"]
        ],
        "solution":"B",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You assign security roles to an owner team and each user on the team. What effect does the security role that is assigned to the team have on the individual team members",
        "choix":[
            ["A. The permissions for the user are used instead of those of the team."],
            ["B. The user is granted the least restrictive permissions of the two roles."],
            ["C. The permissions for the team are used instead of those of the user."],
            ["D. The user is granted the most restrictive permissions of the two roles"]
        ],
        "solution":"B",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You create a workflow and set the scope to User (default). User A owns the workflow. User B is making bulk changes on records that will trigger the workflow. Which records will this workflow affect",
        "choix":[
            ["A. records owned by users in the same business unit as User B"],
            ["B. records owned by User A"],
            ["C. records owned by User B"],
            ["D. records owned by either User A or User B"]
        ],
        "solution":"B",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You export five modified system security roles from a development environment as an unmanaged solution. You solution into a production environment. Which of the following statements is true regarding the import of the unmanaged solution",
        "choix":[
            ["A. Copies of security roles will be created automatically in the production environment."],
            ["B. The system security roles in production will be overwritten with the settings imported from the solution."],
            ["C. Security roles cannot be imported."],
            ["D. The import will fail because system security roles cannot be overwritten by importing a solution"]
        ],
        "solution":"B",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You need to add the status reason Duplicate to the status value Cancelled for the case entity. What should you do",
        "choix":[
            ["A. Add the value to the Cancelled list of the Status field."],
            ["B. Add the value to the Cancelled list of the Status Reason field."],
            ["C. Using status reason transitions, add the value to the Cancelled status."],
            ["D. Using status reason transitions, add the value to the Cancelled status reason"]
        ],
        "solution":"B",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You need to create a process that prompts users with questions and captures their answers. Which process type should you use?",
        "choix":[
            ["A. business rule"],
            ["B. dialog"],
            ["C. custom action"],
            ["D. background workflow"]
        ],
        "solution":"B",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You plan to use entity mapping to replicate data from a text field between two related entities. For which scenario can you replicate data",
        "choix":[
            ["A. The target field is already mapped to a different source field."],
            ["B. The length of the source field is smaller than the destination field."],
            ["C. The two entities are in a N:N relationship."],
            ["D. The new record is created and then related to the parent record"]
        ],
        "solution":"B",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"When a business process flow (BPF) enters a specific stage, you need to run a workflow. Which two actions should you perform? Each correct answer presents part of the solution",
        "choix":[
            ["A. Add a global workflow composition component."],
            ["B. Select an on-demand workflow to run."],
            ["C. Add a stage specific workflow composition component."],
            ["D. Select a step to run."]
        ],
        "solution":"BC",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"Which two features are only available for real-time workflows?",
        "choix":[
            ["A. Start before a record is created."],
            ["B. Wait six hours before executing the next step."],
            ["C. Start before a record is assigned."],
            ["D. Run as the user who made changes to the record."]
        ],
        "solution":"BC",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You are implementing a new business process flow (BPF) in Microsoft Dynamics 365. You specify a name and the primary intent for the BPF, and define the stages and conditions. You need to configure the composition. Which two options can you configure? Each correct answer presents a complete solution",
        "choix":[
            ["A. Dialogs"],
            ["B. Workflows"],
            ["C. Steps"],
            ["D. Actions"]
        ],
        "solution":"BC",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You need to provide users that have the Sales manager security role access to multiple business process flows (BPFs). Which two statements regarding role driven BPFs are true",
        "choix":[
            ["A. Users can select a default BPF for all records."],
            ["B. If a user does not have access to the current process that is applied to a record, options will be disabled."],
            ["C. Set Order defines the order in which BPFs are viewed."],
            ["D. You must use teams to control which groups of users have access to a BPF."]
        ],
        "solution":"BC",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You need to use Theme settings to customize the Microsoft Dynamics 365 user interface for a customer. Which three actions can you perform? Each correct answer presents a complete solution",
        "choix":[
            ["A. Export a theme as part of a solution."],
            ["B. Adjust accent colors including hover and selection colors."],
            ["C. Change the logo and navigation colors."],
            ["D. Change icon colors."],
            ["E. Provide entity-specific coloring."]
        ],
        "solution":"BCE",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"Which two of the following are valid security role permissions? Each correct answer presents a complete solution",
        "choix":[
            ["A. Deactivate"],
            ["B. Share"],
            ["C. Edit"],
            ["D. Append"]
        ],
        "solution":"BD",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You have a Microsoft Dynamics 365 environment. You plan to configure folder-level tracking. Which two statements are true? Each correct answer presents a complete solution",
        "choix":[
            ["A. Folder-level tracking is available for mailboxes that implement delegate access."],
            ["B. Each user can create a maximum of 25 folders in their inbox."],
            ["C. Folder level-tracking works with the Microsoft Dynamics 365 email router."],
            ["D. You cannot change the regarding object unless you remove the email from tracked folders"]
        ],
        "solution":"BD",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You need to configure Microsoft OneNote integration for an organization’s Microsoft Dynamics 365 case entity. Which two components should you enable? Each correct answer presents part of the solution",
        "choix":[
            ["A. Microsoft Office 365 groups"],
            ["B. document management for the entity"],
            ["C. Interactive Service Hub"],
            ["D. server-based Microsoft SharePoint integration"],
            ["E. Microsoft OneDrive integration"]
        ],
        "solution":"BD",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You need to ensure that users can save private documents in Microsoft OneDrive for Business folders from within Microsoft Dynamics 365. Which two actions should you perform? Each correct answer presents part of the solution",
        "choix":[
            ["A. Set the Manage User Synchronization Filters option to Origination for the Security Role."],
            ["B. Configure server-based integration for Microsoft Dynamics 365 and Microsoft SharePoint Online."],
            ["C. Install and activate the Microsoft SharePoint List Component."],
            ["D. Ensure that users have Microsoft OneDrive for Business and SharePoint licenses"]
        ],
        "solution":"BD",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"Which three statements regarding form design or behavior for the mobile phone or tablet application are true? Each correct answer presents a complete solution.",
        "choix":[
            ["A. The form selector is available."],
            ["B. The first five tabs will display."],
            ["C. Activities can be modified."],
            ["D. Tabs do not have the expand and collapse capability."],
            ["E. If business rules are changed while the mobile app is open, the app must be closed and reopened for those changes to apply."]
        ],
        "solution":"BDE",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"The receptionist for a company needs to quickly capture basic information regarding visitors. You configure several different quick create forms for the Visitor entity. The receptionist reports that only one of the quick create forms displays. Why are the other quick create forms not showing",
        "choix":[
            ["A. You did not assign the receptionist access to the form."],
            ["B. You did not enable the switch forms control for quick create forms."],
            ["C. You can only show one quick create form per entity."],
            ["D. You did not assign the correct security role for the quick create from."]
        ],
        "solution":"C",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"Users need to be able to embed Microsoft Power Bl visuals into personal dashboards. What should you do?",
        "choix":[
            ["A. Enable server-based integration with Power Bl."],
            ["B. Add Power Bl as a report category."],
            ["C. Enable Power Bl visualization embedding."],
            ["D. Grant users Power Bl reporting permissions."]
        ],
        "solution":"C",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations "
    },
    {
        "question":"What is the character limit for a Single Line of Text field?",
        "choix":[
            ["A. 1,000"],
            ["B. 2,000"],
            ["C. 4,000"],
            ["D. 10,000"]
        ],
        "solution":"C",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"What is the total number of If-Else conditions that you can add to a business rule?",
        "choix":[
            ["A. 5"],
            ["B. 7"],
            ["C. 10"],
            ["D. 15"]
        ],
        "solution":"C",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You add a new entity named Parts to Microsoft Dynamics 365. You need to grant all users that have the Salesperson security role read access to the Parts entity. Which tab of the Security Role page should you use",
        "choix":[
            ["A. Business Management"],
            ["B. Missing Entities"],
            ["C. Custom Entities"],
            ["D. Customization"]
        ],
        "solution":"C",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You added a custom field to the Account entity. The field type is single line of text. You set a 500 character limit. You need to add the field as a column on the Active Accounts view. What is the maximum width in pixels (px) that you can specify for this column",
        "choix":[
            ["A. 200px"],
            ["B. 500px"],
            ["C. 300px"],
            ["D. 1,000px"]
        ],
        "solution":"C",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You are implementing Microsoft Dynamics 365 for a medical center. You need to implement the Connection functionality of Microsoft Dynamics 365 to display family relationships between patients. What should you do",
        "choix":[
            ["A. Create a new entity named Connection Role that allows staff to connect the Contact records for family members."],
            ["B. Configure a 1:N relationship between the Contact entity and Relationship entity."],
            ["C. Specify the Connection Roles that describe the relationship."],
            ["D. Create a new Activity entity to describe the relationship."]
        ],
        "solution":"C",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You create a business rule. Which action can you perform?",
        "choix":[
            ["A. Hide a section."],
            ["B. Show an error message when a rule fails to run."],
            ["C. Show an error message when a condition is met."],
            ["D. Hide a tab."]
        ],
        "solution":"C",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You create a custom field and set the requirement level to Business Required. For which scenario is the requirement level enforced",
        "choix":[
            ["A. A user creates a record through the web interface but the required field is not on the current form."],
            ["B. A user creates a record by using one of the mobile clients."],
            ["C. A developer creates a record by using custom code."],
            ["D. A user creates a record by using the data import functionality."]
        ],
        "solution":"C",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You have a Microsoft Dynamics 365 environment. You create and deploy a custom entity. Which property can you turn off after enabling the property",
        "choix":[
            ["A. Connections"],
            ["B. Feedback"],
            ["C. Change Tracking"],
            ["D. Activities"]
        ],
        "solution":"C",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You implement Microsoft Dynamics 365 for customers. A customer reports that their sales support staff is taking too much time updating the probability on opportunities. You must display an Editable Grid control when users view the Opportunity entity from the web. You must display a read-only grid when users view opportunities on a tablet device. What should you do?",
        "choix":[
            ["A. On the Opportunity entity, clear the Enable for mobile setting."],
            ["B. On the Outlook and Mobile Settings page for the Opportunity entity, configure the Organization data download filter."],
            ["C. On the Opportunity entity, select the Tablet option for the Microsoft Dynamics 365 Read-only Grid setting."],
            ["D. On the Opportunity entity, select the Read-only in mobile setting"]
        ],
        "solution":"C",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You need to create a custom entity to host records that are owned by salespeople in the organization. Which entity type should you use",
        "choix":[
            ["A. Access Team"],
            ["B. Organization "],
            ["C. User or Team "],
            ["D. Owner"]
        ],
        "solution":"C",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You need to install the Microsoft Dynamics 365 App for Outlook on your device. What should you do?",
        "choix":[
            ["A. In Settings, open Apps for Dynamics 365."],
            ["B. Add your user name to the Microsoft Dynamics 365 App for Outlook eligible user list."],
            ["C. Enable the option to automatically add the app to Outlook."],
            ["D. Download and install the app from Microsoft AppSource."]
        ],
        "solution":"C",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You need to model a relationship between the account entity and the contact entity to track the advisory board members of accounts. You need to use connections to model the relationship between the account and contact entities. What should you do",
        "choix":[
            ["A. Create a one-to-many (1:N) relationship between the account and contact entities, and then use two connection roles to represent each side of the new 1:N relationship."],
            ["B. Create a connection entity, and then use two connection roles to represent each entity in the relationship."],
            ["C. Use two connection roles to represent each side of the relationship."],
            ["D. Create a many-to-many (N:N) relationship between the account and contact entities, and then use two connection roles to represent each side of the new N:N relationship."]
        ],
        "solution":"C",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"Your company rents tools and machines to customers. You create a custom entity named Equipment to track the tools and machines. You need to show the hierarchical relationships between various pieces of equipment. How should you configure the relationship",
        "choix":[
            ["A. Ensure that you enable Connections for the entity."],
            ["B. Set up a parental 1:N relationship between Accounts and Equipment."],
            ["C. Create a self-referential relationship and configure hierarchy settings."],
            ["D. Set up a N:N relationship between Accounts and Equipment."]
        ],
        "solution":"C",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"Which two components can you export as part of a solution? Each correct answer presents a complete solution.",
        "choix":[
            ["A. business units"],
            ["B. themes"],
            ["C. web resources"],
            ["D. connection roles"]
        ],
        "solution":"CD",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"Which two field types can you configure as rollup fields? Each correct answer presents a complete solution",
        "choix":[
            ["A. single line of text"],
            ["B. floating point number"],
            ["C. date and time"],
            ["D. decimal number"]
        ],
        "solution":"CD",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"Which two series aggregate functions are only available on numeric field data types? Each correct answer presents part of the solution.",
        "choix":[
            ["A. Count: Non-empty"],
            ["B. Count: All"],
            ["C. Avg"],
            ["D. Max"]
        ],
        "solution":"CD",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You create a Publisher and add entities to a solution. What are two outcomes of this action? Each correct answer presents a completes solution",
        "choix":[
            ["A. The name for existing custom entities that you add to a solution use the prefix that indicates the publisher."],
            ["B. A new section dedicated to the publisher is added to the sitemap."],
            ["C. Option sets that you create as part of the solution use the prefix that indicates the publisher."],
            ["D. The name for custom entities that you create as part of the solution use the prefix that indicates the publisher."]
        ],
        "solution":"CD",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You have a custom entity. You need to add a date field that displays the date a user was hired. You must ensure that the value in the field remains the same, regardless of the time zone in which the field is viewed. What are two possible ways to configure the field? Each correct answer presents a complete solution",
        "choix":[
            ["A. Set the Behavior to User Local."],
            ["B. Set the Format to Date Only."],
            ["C. Set the Behavior to Time-Zone Independent."],
            ["D. Set the Behavior to Date Only."]
        ],
        "solution":"CD",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You are creating a set of system views. Which three options can you configure for the views? Each correct answer presents a complete solution",
        "choix":[
            ["A. read, write, and delete permissions"],
            ["B. append, assign, and share permissions"],
            ["C. default sort order for results"],
            ["D. the widths of each column"],
            ["E. the columns to display"]
        ],
        "solution":"CDE",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":" You use access teams to assign security permissions. Which of the following statements is true",
        "choix":[
            ["A. Permissions are defined by an access team template  that is associated with each record."],
            ["B. Permissions are defined by an access team template that spans multiple entities."],
            ["C. Permissions are defined by the security role that is assigned to the access team."],
            ["D. Permissions are defined by one of the access team templates for the entity"]
        ],
        "solution":"D",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"A sales team member creates a chart. You need to make the chart available to all members of the sales team. What should you do",
        "choix":[
            ["A. Download and use the Microsoft Dynamics CRM 2016 Report Authoring Extension."],
            ["B. Ask the team member to export the data used by the chart."],
            ["C. Export the chart XML and import it into system charts."],
            ["D. Ask the team member to share the chart with other sales team members"]
        ],
        "solution":"D",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"Which action can you perform by using a business rule?",
        "choix":[
            ["A. Subtract $5.00 from a currency field."],
            ["B. Clear a two options field."],
            ["C. Concatenate two text fields."],
            ["D. Add six days to a date field."]
        ],
        "solution":"D",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"Which component can you add to a solution?",
        "choix":[
            ["A. Goals"],
            ["B. Queues"],
            ["C. Subjects"],
            ["D. Processes"]
        ],
        "solution":"D",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You are designing a managed solution that will be deployed to another part of the business. Users may want to customize specific parts of the solution after the solution is installed. Which capability represents a managed property that users can configure",
        "choix":[
            ["A. the ability to change the display name of a system entity"],
            ["B. the ability to reassign a system dashboard"],
            ["C. the ability to rename a web resource"],
            ["D. the ability to add forms to a custom entity"]
        ],
        "solution":"D",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You are implementing a Lead to Quote business process. The process will update the Lead, Account, Contact, and Quote entities. You need to create a business process flow (BPF) that spans the entities. What should you do",
        "choix":[
            ["A. For each stage, specify the step."],
            ["B. Add a workflow."],
            ["C. Use a composition."],
            ["D. Add a stage and specify the properties for each entity."]
        ],
        "solution":"D",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You create a custom entity. Records are created by users through the user interface, and through a backend integration. You must combine two fields to populate a new field that will replace the name field on the entity. You need to ensure that the new field displays a result immediately after a new record is saved. What should you use",
        "choix":[
            ["A. a background workflow"],
            ["B. JavaScript code on the record form"],
            ["C. a form-scoped business rule"],
            ["D. a calculated field"]
        ],
        "solution":"D",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You create a new custom entity and add it to the sitemap. System administrators can see the custom entity. Users report that they cannot see the custom entity. You need to ensure that users can see the custom entity. What should you do",
        "choix":[
            ["A. Publish the sitemap."],
            ["B. Publish the custom entity."],
            ["C. Wait for changes to the security roles to take effect."],
            ["D. Grant appropriate security roles access to the custom entity."]
        ],
        "solution":"D",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You create a workflow that automatically sends an email when a condition is met. The staff reports that emails are not been sent as expected. You need to correct the issue. What should you do",
        "choix":[
            ["A. Export the workflow."],
            ["B. Publish the workflow."],
            ["C. Set the workflow to required."],
            ["D. Activate the workflow."]
        ],
        "solution":"D",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You create an asynchronous workflow that runs on demand. Which statement is true",
        "choix":[
            ["A. The workflow will run in the security context of the user who is designated in the Execute As setting."],
            ["B. The workflow will run in the security context of the system evaluating all conditions and taking all actions configured."],
            ["C. The workflow will run in the security context of the workflow owner."],
            ["D. The workflow will run in the security context of the user who triggered it."]
        ],
        "solution":"D",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You create an organization-owned custom entity. You need to ensure that users can see the custom entity while minimizing the permissions that you grant users. What should you do",
        "choix":[
            ["A. Ask a system administrator to share the records with the users who need access."],
            ["B. Grant access to all users and then remove the entity from the sitemap and advanced find. Provide users who need access a direct link to a view of the entity."],
            ["C. Grant the appropriate security roles user-level access to the custom entity."],
            ["D. Grant the appropriate security roles organization-level access to the custom entity"]
        ],
        "solution":"D",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You enable folder-level tracking functionality for all users. A user named User1 creates a folder named Dynamics 365 underneath her in box. User1 moves emails into the Dynamics 365 folder. User1 reports that the emails are not being tracked. You need to resolve the issue. What should you do",
        "choix":[
            ["A. Ensure that the Microsoft Dynamics 365 for Outlook client is installed."],
            ["B. Configure server-side synchronization for Microsoft Exchange."],
            ["C. Ensure that Microsoft Exchange rules are created."],
            ["D. Instruct the user to create a folder tracking rule."]
        ],
        "solution":"D",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You have a Dynamics CRM development environment and a Dynamics CRM production environment. You have a project solution in the development environment. You need to import the solution to the production environment as a managed solution. What should you do",
        "choix":[
            ["A. Change the options of the Import solution dialog box to import the solution as managed."],
            ["B. Use the Package Deployer for Microsoft Dynamics CRM to import the unmanaged solution to CRM as managed."],
            ["C. Ask a developer to change the options of the import API so that CRM imports the unmanaged solution as managed."],
            ["D. Export an unmanaged solution and select Managed for the package type. Import the exported solution to the production environment"]
        ],
        "solution":"D",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You have a Dynamics CRM organization. You need to ensure that sales users can access the organization by using the Microsoft Dynamics CRM Windows Store app. What should you do",
        "choix":[
            ["A. Modify the System Settings for the organization."],
            ["B. Assign the Delegate user role to the users."],
            ["C. Configure a server profile."],
            ["D. Enable the CRM for mobile security privilege for a security role"]
        ],
        "solution":"D",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You install a unmanaged solution named SolutionA that has a dependency on another unmanaged solution named SolutionB. What is the outcome when you attempt to uninstall SolutionB?",
        "choix":[
            ["A. solutionB and all components that are not dependencies of solutionA are removed."],
            ["B. The system prevents you from deleting solutio"],
            ["B."],
            ["C. solutionB and all related components are removed."],
            ["D. The container for solutionB is removed, but all its components remain"]
        ],
        "solution":"D",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You manage Microsoft Office 365 and Microsoft Dynamics 365 for a company. Some Office 365 users do not have a Microsoft Dynamics 365 license. You need to ensure that all users can see tasks that are associated with a Microsoft Dynamics 365 opportunity record. What should you implement",
        "choix":[
            ["A. Microsoft OneNote integration"],
            ["B. Microsoft OneDrive integration"],
            ["C. Microsoft Share Point integration"],
            ["D. Microsoft Office 365 groups"]
        ],
        "solution":"D",
        "type": "Configure Microsoft Dynamics 365"
    },
    {
        "question":"You need to create a business process flow (BPF) that spans multiple entities. Which of the following statements is true",
        "choix":[
            ["A. You can use entities that have N:N relationships."],
            ["B. The same entity cannot be used twice."],
            ["C. Relationships must exist between entities."],
            ["D. You can include a maximum of five entities."]
        ],
        "solution":"D",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You need to create a business rule for the account entity that runs both client and server side. Which business rule scope should you use",
        "choix":[
            ["A. All forms"],
            ["B. Account"],
            ["C. Account for Interactive Experience"],
            ["D. Entity"]
        ],
        "solution":"D",
        "type": "Implement business rules, workflows, and business process flows"
    },
    {
        "question":"You plan to delete a custom entity from an unmanaged solution. Which of the following statements is true",
        "choix":[
            ["A. You must delete the solution that contains the entity."],
            ["B. You can only delete the entity from managed solutions."],
            ["C. You must delete all records related to the entity from the database."],
            ["D. You must remove any dependencies with other objects"]
        ],
        "solution":"D",
        "type": "Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations"
    },
    {
        "question":"You plan to integrate data from an external system into Microsoft Dynamics 365. You need to create a Microsoft Dynamics 365 field to use for matching records from the external system. What should you create",
        "choix":[
            ["A. calculated field"],
            ["B. global option set"],
            ["C. rollup field"],
            ["D. alternate key"]
        ],
        "solution":"D",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    },
    {
        "question":"You enable status reason transitions for the case entity. You cannot reactivate a case due to an issue with the current status reason. You need to correct the issue. What should you do",
        "choix":[
            ["A. Using status reason transitions, ensure that all active status reason options have at least one value."],
            ["B. Using status reason transitions, add a new active status reason to the current status reason for the case."],
            ["C. Using status reason transitions, ensure that all inactive status reason options have at least one value."],
            ["D. Using status reason transitions, add a new inactive status reason to the current status reason for the case."]
        ],
        "solution":"A",
        "type": "Implement Microsoft Dynamics 365 entities, entity relationships, and fields"
    }
  ]

  constructor() { }

  getListeQuestion() {
    return this.listeQuestion;
  }
}
