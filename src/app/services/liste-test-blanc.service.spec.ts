import { TestBed, inject } from '@angular/core/testing';

import { ListeTestBlancService } from './liste-test-blanc.service';

describe('ListeTestBlancService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListeTestBlancService]
    });
  });

  it('should be created', inject([ListeTestBlancService], (service: ListeTestBlancService) => {
    expect(service).toBeTruthy();
  }));
});
