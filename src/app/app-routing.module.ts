import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// import { QuestionComponent } from './components/question/question.component';
import { HomeComponent } from './components/home/home.component';
import { ListQuestionComponent } from './components/list-question/list-question.component';
import { ListTestBlancComponent } from './components/list-test-blanc/list-test-blanc.component';


const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'listeQuestion', component: ListQuestionComponent },
  { path: 'testBlanc', component: ListTestBlancComponent },
  { path: '**',  redirectTo: '/home' }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
