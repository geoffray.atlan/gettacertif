import { Component, OnInit } from '@angular/core';
import { ListeQuestionService } from '../../services/liste-question.service';


@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.scss']
})
export class ListQuestionComponent implements OnInit {

  listeQuestion: any[] = [];

  constructor(private listeQuestionService: ListeQuestionService) { }

  ngOnInit() {
    this.loadListeQuestion();
    // console.log(this.listeQuestion)
  }

  loadListeQuestion() {
    this.listeQuestion = this.listeQuestionService.getListeQuestion();
  }

}
