import { Component, OnInit, Input } from '@angular/core';
import { ListeTestBlancService } from '../../services/liste-test-blanc.service';

@Component({
  selector: 'app-test-blanc',
  templateUrl: './test-blanc.component.html',
  styleUrls: ['./test-blanc.component.scss']
})
export class TestBlancComponent implements OnInit {

listeQuestionTest: any[] = [];
question: string;
choix: string;
solution: string;
type: string;

  constructor(private listeTestBlancService: ListeTestBlancService) { }

  ngOnInit() {
    // if (this.listeQuestionTest.length == 0) {

    // }
    this.listeQuestionTest = this.listeTestBlancService.listeQuestionTest;
    console.log(this.listeQuestionTest)
  }

}
