import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestBlancComponent } from './test-blanc.component';

describe('TestBlancComponent', () => {
  let component: TestBlancComponent;
  let fixture: ComponentFixture<TestBlancComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestBlancComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestBlancComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
