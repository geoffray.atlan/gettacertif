import { Component, OnInit } from '@angular/core';
import { ListeQuestionService } from '../../services/liste-question.service';
import { ListeTestBlancService } from '../../services/liste-test-blanc.service';

@Component({
  selector: 'app-list-test-blanc',
  templateUrl: './list-test-blanc.component.html',
  styleUrls: ['./list-test-blanc.component.scss']
})
export class ListTestBlancComponent implements OnInit {

  listeQuestion: any[] = [];
  listeQuestionTest: any[] = [];
  numberQuestionType1 = 0;  // Configure Microsoft Dynamics 365 ==> 13
  numberQuestionType2 = 0;  // Implement Microsoft Dynamics 365 entities, entity relationships, and fields ==> 12
  numberQuestionType3 = 0;  // Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations ==> 11
  numberQuestionType4 = 0;  // Implement business rules, workflows, and business process flows ==> 11

  constructor(private listeQuestionService: ListeQuestionService,
              private listeTestBlancService: ListeTestBlancService
  ) { }

  ngOnInit() {
    this.loadListeQuestion();
    this.loadListeQuestionTest();
  }

  loadListeQuestion() {
    this.listeQuestion = this.listeQuestionService.getListeQuestion();
  }

  loadListeQuestionTest() {
    while (this.listeQuestionTest.length < 47) {
      let randomNumber = 0;
      randomNumber = this.getRandomInt(this.listeQuestion.length);
      switch (this.listeQuestion[randomNumber].type) {
        case 'Configure Microsoft Dynamics 365':
          if (this.numberQuestionType1 <= 13) {
            this.addQuestion(randomNumber);
            this.numberQuestionType1++;
          }
          break;
        case 'Implement Microsoft Dynamics 365 entities, entity relationships, and fields':
          if (this.numberQuestionType2 <= 12) {
            this.addQuestion(randomNumber);
            this.numberQuestionType2++;
          }
          break;
        case 'Create and manage Microsoft Dynamics 365 solutions, forms, views, and visualizations':
          if (this.numberQuestionType3 <= 11) {
            this.addQuestion(randomNumber);
            this.numberQuestionType3++;
          }
          break;
        case 'Implement business rules, workflows, and business process flows':
          if (this.numberQuestionType4 <= 11) {
            this.addQuestion(randomNumber);
            this.numberQuestionType4++;
          }
          break;
        default:
          break;
      }
    }
    this.listeTestBlancService.listeQuestionTest = this.listeQuestionTest;
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  // Ajoute la question au nouveau tableau et la retire de l'ancien
  addQuestion(numberQuestion) {
    this.listeQuestionTest.push({
      'question': this.listeQuestion[numberQuestion].question,
      'choix': this.listeQuestion[numberQuestion].choix,
      'solution': this.listeQuestion[numberQuestion].solution,
      'type': this.listeQuestion[numberQuestion].type
    });
    this.listeQuestion.splice(numberQuestion, 1);
  }
}
