import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTestBlancComponent } from './list-test-blanc.component';

describe('ListTestBlancComponent', () => {
  let component: ListTestBlancComponent;
  let fixture: ComponentFixture<ListTestBlancComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTestBlancComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTestBlancComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
