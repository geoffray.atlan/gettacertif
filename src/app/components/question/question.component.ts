import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  displayAnswerAsked: boolean = false;

  @Input() question: string;
  @Input() choix: string;
  @Input() solution: string;
  @Input() numeroQuestion: number;
  @Input() type: string;


  constructor() { }

  ngOnInit() {
    // console.log(this.choix)
  }

  displayAnswer() {
    if (this.displayAnswerAsked === true) {
      this.displayAnswerAsked = false;
    } else {
      this.displayAnswerAsked = true;
    }
  }

}
