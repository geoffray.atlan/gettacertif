import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { QuestionComponent } from './components/question/question.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { ListQuestionComponent } from './components/list-question/list-question.component';
import { TestBlancComponent } from './components/test-blanc/test-blanc.component';
import { ListTestBlancComponent } from './components/list-test-blanc/list-test-blanc.component';

import { ListeQuestionService } from './services/liste-question.service';
import { ListeTestBlancService } from './services/liste-test-blanc.service';


@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    NavbarComponent,
    HomeComponent,
    ListQuestionComponent,
    TestBlancComponent,
    ListTestBlancComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    ListeQuestionService,
    ListeTestBlancService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
